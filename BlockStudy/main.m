//
//  main.m
//  BlockStudy
//
//  Created by xiaohongjun on 2016/11/8.
//  Copyright © 2016年 xiaohongjun. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int i = 10;
        void (^block)() = ^{i;};
        
        NSObject *o = [NSObject new];
        void (^test)(void) = ^{
//            o.description = @"je";
        };
        
//        __weak void (^weakBlock)() = ^{i;};
//        
//        void (^stackBlock)() = ^{};
        
        // ARC情况下
        
        // 创建时，都会在栈中
        // <__NSStackBlock__: 0x7fff5fbff730>
//        NSLog(@"%@", ^{i;});
        
        // 因为stackBlock为strong类型，且捕获了外部变量，所以赋值时，自动进行了copy
        // <__NSMallocBlock__: 0x100206920>
        NSLog(@"%@", block);
        
        // 如果是weak类型的block，依然不会自动进行copy
        // <__NSStackBlock__: 0x7fff5fbff728>
//        NSLog(@"%@", weakBlock);
        
        // 如果block是strong类型，并且没有捕获外部变量，那么就会转换成__NSGlobalBlock__
        // <__NSGlobalBlock__: 0x100001110>
//        NSLog(@"%@", stackBlock);
        
    }
    return 0;
}
